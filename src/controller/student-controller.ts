import  { ModelView, controller, routeMap, RoutingService } from "space-mvc"


import AddStudentComponent from '../components/student/add-student.f7.html'


import { StudentService } from "../service/student-service"
import { Student } from "../dto/student"


@controller()
class StudentController {

    constructor(
        private studentService:StudentService,
        private routingService:RoutingService
    ) {}

    @routeMap("/student")
    async showIndex(): Promise<ModelView> {
        
        return new ModelView(async () => {

            return {
                list: await this.studentService.list() 
            }

        })

    }

    @routeMap("/student/add")
    async addStudent(): Promise<ModelView> {
        return new ModelView(async () => {
        }, AddStudentComponent)

    }


    @routeMap("/student/save")
    async saveStudent(): Promise<ModelView> {

        return new ModelView(async (student:Student) => {

            await this.studentService.put(student.walletAddress, student)

            this.routingService.navigate({
                path: "/"
            })
        })


    }

}

export { StudentController }
