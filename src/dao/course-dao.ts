import { dao, SchemaService, Initializable } from "space-mvc";
import { Course } from "../dto/course";

@dao()
class CourseDao implements Initializable {

    private store

    constructor(
        private schemaService:SchemaService
    ) {}

    async init() {
        this.store = this.schemaService.getStore("course") 
    }

    async put(key:string, student:Course) {
        return this.store.put(key, student)
    }

    async get(key:string): Promise<Course> {
        return this.store.get(key)
    }

    async list(offset?: number, limit?: number) : Promise<Course[]> {
        return this.store.list(offset, limit)
    }

}

export {
    CourseDao
}