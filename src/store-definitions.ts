import { StoreDefinitions } from "space-mvc";

let mainStoreDefinitions: StoreDefinitions[] = [

    {
        name: "student",
        type: "mfsstore",
        load: 100,
        schema: {
            walletAddress: { unique: true },
            firstName: { unique: false },
            lastName: { unique: false }
        }
    },

    {
        name: "course",
        type: "mfsstore",
        load: 100,
        schema: {
            founderWallet: { unique: false },
            name: { unique: false }
        }
    }


]




export {
    mainStoreDefinitions
}