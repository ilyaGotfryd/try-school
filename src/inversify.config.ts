import { Container } from "space-mvc"
import { HomeController } from "./controller/home-controller"
import { StudentService } from "./service/student-service"
import { StudentDao } from "./dao/student-dao"
import { CourseController } from "./controller/course-controller"

const container = new Container()

//Bind our home controller
container.bind(HomeController).toSelf().inSingletonScope()
container.bind(CourseController).toSelf().inSingletonScope()


//Bind the service and DAO for person
container.bind(StudentService).toSelf().inSingletonScope()
container.bind(StudentDao).toSelf().inSingletonScope()

container.bind("ipfsOptions").toConstantValue(
    {
        repo: `try-school-repo`,
        EXPERIMENTAL: {
            ipnsPubsub: true
        },
        preload: {
            enabled: false
        },
        relay: {
            enabled: true,
            hop: {
                enabled: true // enable circuit relay HOP (make this node a relay)
            }
        },
        config: {
            Addresses: {
                Swarm: [
                    '/dns4/secure-beyond-12878.herokuapp.com/tcp/443/wss/p2p-webrtc-star/',
                    '/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
                    '/dns4/wrtc-star2.sjc.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
                    '/dns4/webrtc-star.discovery.libp2p.io/tcp/443/wss/p2p-webrtc-star/',
                    '/dns4/libp2p-rdv.vps.revolunet.com/tcp/443/wss/p2p-webrtc-star/'
                ]
            }
        }
    }
)


export {
    container
}