import { service } from "space-mvc";
import { StudentDao } from "../dao/student-dao";
import { Student } from "../dto/student";

@service()
class StudentService {

    constructor(
        private studentDao:StudentDao
    ) {}

    async put(key:string, student:Student) {
        return this.studentDao.put(key, student)
    }

    async get(key:string): Promise<Student> {
        return this.studentDao.get(key)
    }

    async list(offset?: number, limit?: number) : Promise<Student[]> {
        return this.studentDao.list(offset, limit)
    }


}

export {
    StudentService
}