import { service } from "space-mvc";
import { CourseDao } from "../dao/course-dao";
import { Course } from "../dto/course";

@service()
class CourseService {

    constructor(
        private courseDao:CourseDao
    ) {}

    async put(key:string, student:Course) {
        return this.courseDao.put(key, student)
    }

    async get(key:string): Promise<Course> {
        return this.courseDao.get(key)
    }

    async list(offset?: number, limit?: number) : Promise<Course[]> {
        return this.courseDao.list(offset, limit)
    }


}

export {
    CourseService
}