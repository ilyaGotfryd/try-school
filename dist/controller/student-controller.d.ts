import { ModelView, RoutingService } from "space-mvc";
import { StudentService } from "../service/student-service";
declare class StudentController {
    private studentService;
    private routingService;
    constructor(studentService: StudentService, routingService: RoutingService);
    showIndex(): Promise<ModelView>;
    addStudent(): Promise<ModelView>;
    saveStudent(): Promise<ModelView>;
}
export { StudentController };
