import { StudentDao } from "../dao/student-dao";
import { Student } from "../dto/student";
declare class StudentService {
    private studentDao;
    constructor(studentDao: StudentDao);
    put(key: string, student: Student): Promise<any>;
    get(key: string): Promise<Student>;
    list(offset?: number, limit?: number): Promise<Student[]>;
}
export { StudentService };
