import "core-js/stable";
import "regenerator-runtime/runtime";
import './html/css/framework7.bundle.min.css';
import './html/css/framework7-icons.css';
import './html/css/app.css';
declare const _default: () => Promise<void>;
export default _default;
