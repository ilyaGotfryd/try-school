import assert from 'assert'

import { getTestContainer } from "../test-inversify.config"
import { StudentDao } from "../../src/dao/student-dao"
import { Student } from "../../src/dto/student"

describe('StudentDao', async () => {

    let studentDao: StudentDao


    before('Before', async () => {

        let container = await getTestContainer()
        studentDao = container.get(StudentDao)

    })    

    it("should create and read a student", async () => {
        
        //Arrange
        let person:Student = {
            firstName: "Patrick",
            lastName: "Toner"
        }

        //Act
        await studentDao.put("1", person)

        //Assert

        let savedStudent = await studentDao.get("1")

        assert.equal(person.firstName, savedStudent.firstName)
        assert.equal(person.lastName, savedStudent.lastName)
        
    })

    it("should update an existing student", async () => {
        
        //Arrange
        let person:Student = {
            firstName: "John",
            lastName: "Davey"
        }

        //Act
        await studentDao.put("1", person)

        //Assert

        let savedStudent = await studentDao.get("1")

        assert.equal(person.firstName, savedStudent.firstName)
        assert.equal(person.lastName, savedStudent.lastName)
        
    })


    it("should get a list of students", async () => {

        //Act
        for (let i=0; i < 10; i++) {
            await studentDao.put(i.toString(), { 
                firstName: `First${i.toString()}`, 
                lastName: `Last${i.toString()}` 
            })
        }

        //Get a couple of lists
        let list = await studentDao.list(0,10)
        let list2 = await studentDao.list(5,5)

        //Assert
        assert.equal(list.length, 10)
        assert.equal(list[9].firstName, "First9")

        assert.equal(list2.length, 5)
        assert.equal(list2[4].firstName, "First9")

    })

})
