import { merge } from 'webpack-merge'
import common from './webpack.common'

export default merge(common, {
    mode: 'production',
    optimization: {
        splitChunks: {
            chunks: 'async',
            minSize: 20000,
            maxSize: 1000000
          }
    }
})